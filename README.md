# Info Globo Backend Challenge #

Info Globo Backend Challenge is an application to manage as rss information.


### Setup Dev###

* virtualenv -p python3 .venv
* source .venv/bin/activate
* pip install -r requirements.txt
*  ./manager.py runserver --reload


### Tests ###

* virtualenv -p python3 .venv
* source .venv/bin/activate
* make test


### Demo ###

* Link : https://infoglobo-backend-challenge.herokuapp.com
* User: admin
* Password: password


### WIKI ###

* https://github.com/CircleCI-Public/circleci-demo-python-flask